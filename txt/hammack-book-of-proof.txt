# offset: 10;
Contents -6
Preface	-3
Introduction	-2
I Fundamentals
 1. Sets	3
  1.1. Introduction to Sets	3
  1.2. The Cartesian Product	8
  1.3. Subsets	11
  1.4. Power Sets	14
  1.5. Union, Intersection, Diﬀerence	17
  1.6. Complement	19
  1.7. Venn Diagrams	21
  1.8. Indexed Sets	24
  1.9. Sets that Are Number Systems	28
  1.10. Russell’s Paradox	29
 2. Logic	31
  2.1. Statements	32
  2.2. And, Or, Not	36
  2.3. Conditional Statements	39
  2.4. Biconditional Statements	42
  2.5. Truth Tables for Statements	44
  2.6. Logical Equivalence	47
  2.7. Quantiﬁers	49
  2.8. More on Conditional Statements	52
  2.9. Translating English to Symbolic Logic	53
  2.10. Negating Statements	55
  2.11. Logical Inference	59
  2.12. An Important Note	60
 3. Counting	61
  3.1. Counting Lists	61
  3.2. Factorials	68
  3.3. Counting Subsets	71
  3.4. Pascal’s Triangle and the Binomial Theorem	76
  3.5. Inclusion-Exclusion	79
II How to Prove Conditional Statements
 4. Direct Proof	85
  4.1. Theorems	85
  4.2. Deﬁnitions	87
  4.3. Direct Proof	90
  4.4. Using Cases	96
  4.5. Treating Similar Cases	97
 5. Contrapositive Proof	100
  5.1. Contrapositive Proof	100
  5.2. Congruence of Integers	103
  5.3. Mathematical Writing	105
 6. Proof by Contradiction	109
  6.1. Proving Statements with Contradiction	110
  6.2. Proving Conditional Statements by Contradiction	113
  6.3. Combining Techniques	114
  6.4. Some Words of Advice	115
III More on Proof
 7. Proving Non-Conditional Statements	119
  7.1. If-and-Only-If Proof	119
  7.2. Equivalent Statements	121
  7.3. Existence Proofs; Existence and Uniqueness Proofs	122
  7.4. Constructive Versus Non-Constructive Proofs	126
 8. Proofs Involving Sets	129
  8.1. How to Prove a ∈ A	129
  8.2. How to Prove A ⊆ B	131
  8.3. How to Prove A = B	134
  8.4. Examples: Perfect Numbers	137
 9. Disproof	144
  9.1. Counterexamples	146
  9.2. Disproving Existence Statements	148
  9.3. Disproof by Contradiction	150
 10. Mathematical Induction	152
  10.1. Proof by Strong Induction	159
  10.2. Proof by Smallest Counterexample	163
  10.3. Fibonacci Numbers	165
IV Relations, Functions and Cardinality
 11. Relations	173
  11.1. Properties of Relations	177
  11.2. Equivalence Relations	182
  11.3. Equivalence Classes and Partitions	186
  11.4. The Integers Modulo n	189
  11.5. Relations Between Sets	192
 12. Functions	194
  12.1. Functions	194
  12.2. Injective and Surjective Functions	199
  12.3. The Pigeonhole Principle	203
  12.4. Composition	206
  12.5. Inverse Functions	209
  12.6. Image and Preimage	212
 13. Cardinality of Sets	215
  13.1. Sets with Equal Cardinalities	215
  13.2. Countable and Uncountable Sets	221
  13.3. Comparing Cardinalities	226
  13.4. The Cantor-Bernstein-Schröeder Theorem	230
Conclusion	237
Solutions	238
Index	299
