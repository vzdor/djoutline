require 'pp'

class Tree
  PAGE_NUM_RE = /(?:-|\d)*/
  LINE_RE = /\A
        (\s*)
        ((?:
            (?!
               (?:\s+#{PAGE_NUM_RE}$)).)+)
        \s*(#{PAGE_NUM_RE})\Z/x

  attr_reader :tree

  def initialize(txt)
    parse(txt)
  end

  protected

  def parse(data)
    lines = data.lines
    ary = lines.map do |line|
      match = line.match(LINE_RE)
      if match
        spaces = match[1].to_s
                 .size
        [spaces, match[2], match[3].to_i]
      end
    end.compact
    make_tree(ary)
  end

  def make_tree(ary)
    @tree = []
    s = [tree]
    ary.each do |lvl, title, page_number|
      lvl += 1
      s.pop while s.size > lvl
      if s.size < lvl
        s << s.last
            .last
      end
      set_page_number(s, page_number) if page_number != 0
      ptr = s.last
      ptr << [title, page_number]
    end
  end

  # Set parent section page number
  def set_page_number(s, page_number)
    i = s.size - 1
    while s[i]
      s[i][1] = page_number if s[i][1] == 0
      i -= 1
    end
  end

end

class SimpleSerializer
  attr_accessor :hints, :offset

  def initialize(tree, offset = nil)
    @tree = tree
    @offset = offset
    parse_hints!
  end

  def outline
    map(@tree)
  end

  protected

  def parse_hints!
    header, _ = @tree.first
    if header.start_with?('#')
      @tree.shift
      @hints = header.scan(/(\w+)\:\s*(\w+)/)
      set_options_from_hints
    end
  end

  def set_options_from_hints
    @hints.each do |hint, value|
      case hint
      when "offset"
        @offset = value.to_i
      end
    end
  end

  def next_level(level)
    level + 2
  end

  def begin_block(title, page, lvl)

  end

  def end_block
    ''
  end

  def map(tree, lvl = 1)
    tree.map do |node|
      title, page, *tail = node
      txt = begin_block(title, page + @offset, lvl)
      unless tail.empty?
        txt << map(tail, next_level(lvl))
      end
      txt + end_block
    end.join
  end
end

# djvu bookmarks format description:
#   http://www.ub-filosofie.ro/~solcan/wt/gnu/d/bdjv.html
class Serializer < SimpleSerializer
  def outline
    "(bookmarks" + map(@tree) + end_block + "\n"
  end

  protected

  def begin_block(title, page, lvl)
    title = title.gsub(/"/, '\\\"')
    "\n#{' ' * lvl}" + # indent
      "(" + "\"#{title}\" \"\##{page}\""
  end

  def end_block
    ')'
  end
end

# pdftk how-to with format description:
#  https://www.pdflabs.com/blog/export-and-import-pdf-bookmarks/
class PdfTkSerializer < SimpleSerializer
  protected

  def next_level(level)
    level + 1
  end

  def begin_block(title, page, lvl)
    txt =<<EOT
BookmarkBegin
BookmarkTitle: #{title}
BookmarkLevel: #{lvl}
BookmarkPageNumber: #{page}
EOT
  end
end

if __FILE__ == $0
  tree = Tree.new(File.read(ARGV.first))
  txt = Serializer.new(tree.tree)
        .outline
  # txt = PdfTkSerializer.new(tree.tree)
  #       .outline
  puts txt
  # File.open("/tmp/outline.txt", "w") { |f| f.write(txt) }
end
