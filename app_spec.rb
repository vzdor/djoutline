require_relative 'app'

module RSpec
  describe Tree do
    it 'parses simple text' do
      txt =<<EOT
Section
 1. hello 10
 2. world 20

Index 101
EOT
      tree = Tree.new(txt).tree
      expect(tree).to eq([['Section', 10,
                           ['1. hello', 10],
                           ['2. world', 20]],
                          ['Index', 101]])
    end

    it 'sets section page number' do
      txt =<<EOT
Section
 1. hello 10
 2. world 20
EOT
      tree = Tree.new(txt).tree
      expect(tree).to eq([['Section', 10,
                           ['1. hello', 10],
                           ['2. world', 20]]])
    end
  end

  describe Serializer do
    describe '#outline' do
      it 'serializes nicely' do
        tree = [['Welcome', 0,
                 ['hello', 1],
                 ['world', 2]]]
        txt =<<EOT
(bookmarks
 ("Welcome" "#0"
   ("hello" "#1")
   ("world" "#2")))
EOT
        s = Serializer.new(tree, 0)
        expect(s.outline).to eq(txt)
      end
    end

    context 'with hints' do
      it 'sets offset from hints' do
        tree = [['# offset: 10;', 0],
              ['hello', 1]]
        s = Serializer.new(tree)
        expect(s.offset).to eq(10)
      end
    end
  end

  describe PdfTkSerializer do
    describe '#outline' do
      it 'works' do
        txt =<<EOT
BookmarkBegin
BookmarkTitle: Welcome
BookmarkLevel: 1
BookmarkPageNumber: 1
BookmarkBegin
BookmarkTitle: hello world
BookmarkLevel: 2
BookmarkPageNumber: 1
EOT
        tree = [['Welcome', 1,
                 ['hello world', 1]]]
        s = PdfTkSerializer.new(tree, 0)
        expect(s.outline).to eq(txt)
      end
    end
  end
end
